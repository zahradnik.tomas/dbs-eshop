<?php

require_once 'php/Db_connection.php';
require_once 'php/Session.php';
require_once 'php/components.php';

try {
    $connection = new Db_connection();
} catch (PDOException $e) {
    echo "Connection error: " . $e->getMessage();
}

?>

<!doctype html>
<html lang="en">

<head>
    <title>Dokončit objednávku</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="css/all.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/custom.css">
</head>

<body>

<!-- HEADER -->
<header>
    <nav class="navbar navbar-expand-md navbar-dark bg-dark">
        <!-- USER  -->
        <div class="pos-f-t container d-md-none">
            <div class="collapse" id="navbarToggleUserMenu">
                <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                    <li class="nav-item">
                        <a class="nav-link" href="udaje.php">Moje údaje</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="objednavky.php">Objednávky</a>
                    </li>

                    <li class="nav-item">
                        <a href="kosik.php" class="nav-link active">
                            <i class="fa-shopping-basket fas fa-lg"></i>
                            Košík
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="index.php">
                            <i class="fas fa-sign-out-alt"></i>
                            Odhlásit se
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="container">
            <!-- Logo -->
            <a class="navbar-brand" href="index.php">Knihomol</a>
            <!-- Nav controll icons -->
            <div class="btn-group">
                <button class="navbar-toggler navbar-user-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarToggleUserMenu" aria-controls="navbarToggleUserMenu" aria-expanded="false"
                        aria-label="Toggle navigation">
                    <i class="fas fa-user"></i>
                </button>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavId"
                        aria-controls="collapsibleNavId" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </div>
            <!-- MAIN NAV COLLAPSE -->
            <div class="collapse navbar-collapse" id="collapsibleNavId">
                <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                    <li class="nav-item">
                        <a class="nav-link" href="index.php">Domů <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="dropdownId" data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false">Kategorie</a>
                        <div class="dropdown-menu" aria-labelledby="dropdownId">
                            <a class="dropdown-item" href="#">Román</a>
                            <a class="dropdown-item" href="#">Krimi</a>
                            <a class="dropdown-item" href="#">Sci-fi</a>
                            <a class="dropdown-item" href="#">Dokumentární</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">Všechny Kategorie</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Kontakt</a>
                    </li>
                </ul>

                <ul class="navbar-nav ml-auto d-md-flex d-sm-none">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-user"></i>
                            Jmeno Zakaznika
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="udaje.php">Moje údaje</a>
                            <a class="dropdown-item" href="objednavky.php">Objednávky</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="index.php">
                                <i class="fas fa-sign-out-alt"></i>
                                Odhlásit se
                            </a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a href="kosik.php" class="nav-link active">
                            <i class="fa-shopping-basket fas fa-lg"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>


<section id="cart-summary" class="bg-red text-white">

    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-6">
                <dl class="row text-left">
                    <dt class="col-sm-4">Počet položek:</dt>
                    <dd class="col-sm-8">123</dd>

                    <dt class="col-sm-4">Celková cena:</dt>
                    <dd class="col-sm-8">15 111 Kč</dd>
                </dl>
            </div>
        </div>
    </div>

</section>

<section id="cart-content">
    <div class="container pt-3 mb-4">
        <h2>Dodací údaje</h2>
        <div class="row">
            <div class="col-sm-12 col-md-8 offset-md-2">
                <form class="mt-4 px-5 py-4 bg-white" action="login.php" method="post">
                    <div class="form-group">
                        <label for="jmeno">Email</label>
                        <input type="email" name="email" class="form-control" id="email" placeholder="Email" value=""
                               required>
                    </div>
                    <div class="form-group">
                        <label for="jmeno">Jméno</label>
                        <input type="text" name="name" class="form-control" id="jmeno" placeholder="Jméno" value=""
                               required>
                    </div>
                    <div class="form-group">
                        <label for="prijmeni">Příjmení</label>
                        <input type="text" name="prijmeni" class="form-control" id="prijmeni" placeholder="Přijmení"
                               value="" required>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-sm-12 col-md-8">
                            <label for="ulice">Ulice</label>
                            <input type="text" name="ulice" placeholder="Ulice" value="" class="form-control" id="ulice"
                                   required>
                        </div>
                        <div class="form-group col-sm-12 col-md-4">
                            <label for="cisloPopisne">Č.p.</label>
                            <input type="text" name="cislo" placeholder="Č.p." value="" class="form-control"
                                   id="cisloPopisne" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="knihaZanr">Město</label>
                        <select name="mesto" class="form-control" id="knihaZanr" required>
                            <?php
                            $cities = $connection->select("SELECT mestaid, mesto, psc FROM mesta");
                            foreach ($cities as $city) {
                                echo '<option value="' . $city['mestaid'] . '">' . $city['psc'] . ' - ' . $city['mesto'] . '</option>';
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="telefon">Telefon</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="inputGroupPrepend">+420</span>
                            </div>
                            <input type="tel" name="telefon" pattern="[0-9]{9}" class="form-control" id="telefon"
                                   placeholder="123456789"
                                   value="" required>
                        </div>
                        <small class="form-text text-muted">
                            Formát čísla: 123456789
                        </small>
                    </div>

                    <button type="submit" class="btn btn-lg btn-dark btn-block">
                        <i class="fas fa-shopping-cart"></i>
                        Odeslat objednávku
                    </button>
                </form>
            </div>
        </div>
    </div>
</section>


<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous">
</script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
</script>
</body>

</html>