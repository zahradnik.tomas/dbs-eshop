<?php

require_once '../../php/Db_connection.php';
require_once '../../php/Session.php';

$session = Session::getInstance();
$redirect = "Location: ../login.php";
$session->adminCheck($redirect);

try {
    $connection = new Db_connection();
} catch (PDOException $e) {
    echo "Connection error: " . $e->getMessage();
}

if (isset($_GET['zakaznik_id'])) {
    $id = $_GET['zakaznik_id'];
} else {
    header('Location: ../zakaznici.php');
}

?>

<!doctype html>
<html lang="en" class="h-100">

<head>
    <title>Detail uživatele</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="../../css/all.css">
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/administration.css">
</head>

<body class="text-center">

<section class="admin-content admin-content-table container">
    <?php
    $customer = $connection->selectOne("SELECT z.zakazniciid, z.email, z.jmeno, z.prijmeni, z.telefon, z.datumnarozeni, a.cislopopisne, a.ulice, a.mestaid, m.mesto, m.psc FROM zakaznici z INNER JOIN adresa a USING (adresaid) INNER JOIN mesta m USING (mestaid) WHERE z.zakazniciid = '$id'");

    echo '
    <h1 class="mb-3 font-weight-normal">Uživatel ' . $customer['zakazniciid'] . '</h1>

    <dl class="row text-left">

        <dt class="col-sm-4">Email:</dt>
        <dd class="col-sm-8">' . $customer['email'] . '</dd>

        <dt class="col-sm-4">Jméno:</dt>
        <dd class="col-sm-8">' . $customer['jmeno'] . '</dd>

        <dt class="col-sm-4">Přijmení:</dt>
        <dd class="col-sm-8">' . $customer['prijmeni'] . '</dd>

        <dt class="col-sm-4">Ulice</dt>
        <dd class="col-sm-8">' . $customer['ulice'] . ' ' . $customer['cislopopisne'] . '</dd>

        <dt class="col-sm-4">Město:</dt>
        <dd class="col-sm-8">' . $customer['psc'] . ' - ' . $customer['mesto'] . '</dd>

        <dt class="col-sm-4">Telefon:</dt>
        <dd class="col-sm-8">' . $customer['telefon'] . '</dd>

        <dt class="col-sm-4">Datum narození:</dt>
        <dd class="col-sm-8">' . date("Y-m-d", strtotime($customer['datumnarozeni'])) . '</dd>
    </dl>
    
    <!-- TABLE -->
    <h2 class="h3">Objednávky</h2>
    <table class="table table-sm table-striped table-responsive-sm">
        <thead>
        <tr>
            <th>ID</th>
            <th>Vyřízeno</th>
            <th>Cena</th>
            <th>Typ platby</th>
            <th>Počet položek</th>
            <th>Datum pořízení</th>
            <th class="col-edit">Detail</th>
        </tr>
        </thead>
        <tbody>';

    $cid = $customer['zakazniciid'];
    $orders = $connection->select("SELECT g.objednavkaid, g.celk_cena, g.zpusob, g.kusu, g.datum, g.zakazniciid, g.zamestnanecid, z.jmeno, z.prijmeni FROM god_view g LEFT JOIN zamestnanec z USING (zamestnanecid) WHERE g.zakazniciid = '$cid'");

    if (count($orders) > 0) {
        foreach ($orders as $order) {
            echo '
                <tr>
                    <th>' . $order['objednavkaid'] . '</th>
                    <td>';
            if (is_null($order['jmeno'])) {
                echo'Nevyřízeno';
            } else {
                echo '' . $order['jmeno'] . ' ' . $order['prijmeni'] . '';
            }
            echo '
                    </td>
                    <td>' . $order['celk_cena'] . '</td>
                    <td>' . $order['zpusob'] . '</td>
                    <td>' . $order['kusu'] . '</td>
                    <td>' . date("Y-m-d", strtotime($order['datum'])) . '</td>
                    <td><a href="objednavka.php?objednavka_id=' . $order['objednavkaid'] . '"><i class="fas fa-info-circle fa-lg text-dark"></i></a></td>
                </tr>
                </tbody>
            </table>';
        }
    }
    ?>

    <!-- PAGINATION -->
    <nav aria-label="Page navigation example">
        <ul class="pagination justify-content-center">
            <li class="page-item disabled">
                <a class="page-link" href="#" tabindex="-1">Previous</a>
            </li>
            <li class="page-item active">
                <a class="page-link" href="#">1</a>
            </li>
            <li class="page-item">
                <a class="page-link" href="#">2</a>
            </li>
            <li class="page-item">
                <a class="page-link" href="#">3</a>
            </li>
            <li class="page-item">
                <a class="page-link" href="#">Next</a>
            </li>
        </ul>
    </nav>
</section>


<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous">
</script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
</script>
</body>

</html>