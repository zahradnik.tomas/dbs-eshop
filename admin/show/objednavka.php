<?php

require_once '../../php/Db_connection.php';
require_once '../../php/Session.php';

$session = Session::getInstance();
$redirect = "Location: ../login.php";
$session->adminCheck($redirect);

try {
    $connection = new Db_connection();
} catch (PDOException $e) {
    echo "Connection error: " . $e->getMessage();
}

if (isset($_GET['objednavka_id'])) {
    $id = $_GET['objednavka_id'];
} else {
    header('Location: ../objednavky.php');
}

?>

<!doctype html>
<html lang="en" class="h-100">

<head>
    <title>Detail objednavky</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="../../css/all.css">
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/administration.css">
</head>

<body class="text-center">

<section class="admin-content admin-content-table container">
    <?php
    if (isset($id)) {
        $order = $connection->selectOne("SELECT o.objednavkaid, o.datum, o.zakazniciid, p.zpusob, z.jmeno, z.prijmeni FROM objednavka o INNER JOIN platba p USING (platbaid) INNER JOIN zamestnanec z USING (zamestnanecid) WHERE o.objednavkaid = '$id'");
        echo '
    <h1 class="mb-3 font-weight-normal">Objednávka</h1>

    <div class="row mb-3">
        <div class="col-sm-12 text-left">
            <h2 class="h4">Informace</h2>
        </div>
        <div class="col-sm-12 col-md-6">
            <dl class="row text-left">
                <dt class="col-sm-4">ID:</dt>
                <dd class="col-sm-8">' . $order['objednavkaid'] . '</dd>

                <dt class="col-sm-4">Objednáno:</dt>
                <dd class="col-sm-8">' . date("Y-m-d H:i:s", strtotime($order['datum'])) . '</dd>

                <dt class="col-sm-4">Typ platby:</dt>
                <dd class="col-sm-8">' . $order['zpusob'] . '</dd>

                <dt class="col-sm-4">Vyrizeno:</dt>
                <dd class="col-sm-8">' . $order['jmeno'] . ' ' . $order['prijmeni'] . '</dd>


            </dl>
        </div>';

        $cid = $order['zakazniciid'];
        $customer = $connection->selectOne("SELECT z.email, z.jmeno, z.prijmeni, z.telefon, z.datumnarozeni, a.cislopopisne, a.ulice, a.mestaid, m.mesto, m.psc FROM zakaznici z INNER JOIN adresa a USING (adresaid) INNER JOIN mesta m USING (mestaid) WHERE z.zakazniciid = '$cid'");

        echo '
    <div class="col-sm-12 col-md-6">
       <dl class="row text-left">

           <dt class="col-sm-4">Email:</dt>
           <dd class="col-sm-8">' . $customer['email'] . '</dd>

           <dt class="col-sm-4">Jméno:</dt>
           <dd class="col-sm-8">' . $customer['jmeno'] . '</dd>

           <dt class="col-sm-4">Přijmení:</dt>
           <dd class="col-sm-8">' . $customer['prijmeni'] . '</dd>

           <dt class="col-sm-4">Ulice</dt>
           <dd class="col-sm-8">' . $customer['ulice'] . ' ' . $customer['cislopopisne'] . '</dd>

           <dt class="col-sm-4">Město:</dt>
           <dd class="col-sm-8">' . $customer['psc'] . ' - ' . $customer['mesto'] . '</dd>

           <dt class="col-sm-4">Telefon:</dt>
           <dd class="col-sm-8">' . $customer['telefon'] . '</dd>

           <dt class="col-sm-4">Narození:</dt>
           <dd class="col-sm-8">' . date("Y-m-d", strtotime($customer['datumnarozeni'])) . '</dd>
        </dl>
    </div>
    </div>
    
    <!-- TABLE -->
    <h2>Zakoupené položky</h2>
    <table class="table table-sm table-striped table-responsive-sm">
        <thead>
        <tr>
            <th>ID</th>
            <th>Název</th>
            <th class="col-edit">Detail</th>
            <th>Cena</th>
            <th>Počet kusů</th>
        </tr>
        </thead>
        <tbody>';

        $oid = $order['objednavkaid'];
        $books = $connection->select("SELECT p.pocet, p.objednavkaid, k.knihyid, k.nazev, k.cena FROM polozky p INNER JOIN knihy k USING (knihyid) WHERE p.objednavkaid = '$oid'");

        if (count($books) > 0) {
            foreach ($books as $book) {
                echo '
        <tr>
            <th>' . $book['knihyid'] . '</th>
            <td>' . $book['nazev'] . '</td>
            <td><a href="../edit/kniha.php?kniha_id=' . $book['knihyid'] . '"><i class="fas fa-pencil-alt"></i></a></td>
            <td>' . $book['cena'] . '</td>
            <td>' . $book['pocet'] . '</td>
        </tr>
                ';
            }
        }
        echo '
        </tbody>
    </table>            
        ';
    }
    ?>

    <!-- PAGINATION -->
    <nav aria-label="Page navigation example">
        <ul class="pagination justify-content-center">
            <li class="page-item disabled">
                <a class="page-link" href="#" tabindex="-1">Previous</a>
            </li>
            <li class="page-item active">
                <a class="page-link" href="#">1</a>
            </li>
            <li class="page-item">
                <a class="page-link" href="#">2</a>
            </li>
            <li class="page-item">
                <a class="page-link" href="#">3</a>
            </li>
            <li class="page-item">
                <a class="page-link" href="#">Next</a>
            </li>
        </ul>
    </nav>
</section>


<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous">
</script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
</script>
</body>

</html>