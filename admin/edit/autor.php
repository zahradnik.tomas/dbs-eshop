<?php

require_once '../../php/Db_connection.php';
require_once '../../php/Session.php';

$session = Session::getInstance();
$redirect = "Location: ../login.php";
$session->adminCheck($redirect);

try {
    $connection = new Db_connection();
} catch (PDOException $e) {
    echo "Connection error: " . $e->getMessage();
}

if (isset($_GET['autor_id'])) {
    $id = $_GET['autor_id'];
} else {
    header('Location: ../autori.php');
}

if (isset($_POST['jmeno']) && isset($_POST['prijmeni']) && isset($_POST['roknarozeni']) && isset($_POST['zanr'])) {
    $name = $_POST['jmeno'];
    $surname = $_POST['prijmeni'];
    $birth = $_POST['roknarozeni'];
    $genreid = $_POST['zanr'];
    $connection->insert("UPDATE autori SET jmeno = '$name', prijmeni = '$surname', roknarozeni = '$birth', zanryid = '$genreid' WHERE autoriid = '$id'");
}

?>

<!doctype html>
<html lang="en" class="h-100">

<head>
    <title>Upravit autora</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="../../css/all.css">
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/administration.css">
</head>

<body>

<section class="admin-content">
    <?php

    if (isset($id)) {
        $author = $connection->selectOne("SELECT a.autoriid, a.jmeno, a.prijmeni, a.roknarozeni, a.zanryid, z.nazev FROM autori a INNER JOIN zanry z USING (zanryid) WHERE a.autoriid = '$id'");
        echo '
        <h1 class="mb-3 font-weight-normal text-center">Autor ' . $author['autoriid'] . '</h1>
        
        <form action="autor.php?autor_id=' . $author['autoriid'] . '" method="post">
            <div class="form-group">
                <label for="autorJmeno">Jméno</label>
                <input type="text" name="jmeno" class="form-control" id="autorJmeno" value="' . $author['jmeno'] . '" required>
            </div>
            <div class="form-group">
                    <label for="autorPrijmeni">Prijmení</label>
                <input type="text" name="prijmeni" class="form-control" id="autorPrijmeni" value="' . $author['prijmeni'] . '" required>
            </div>
            <div class="form-group">
                <label for="autorRokNarozeni">Rok narození</label>
                <input type="number" name="roknarozeni" class="form-control" id="autorRokNarozeni" value="' . $author['roknarozeni'] . '" required>
            </div>
            <div class="form-group">
                <label for="autorZanr">Žánr</label>
                <select name="zanr" class="form-control" id="autorZanr" required>
                    <option value="' . $author['zanryid'] . '">' . $author['nazev'] . '</option>';
            $genres = $connection->select("SELECT zanryid, nazev FROM zanry");
            foreach ($genres as $genre) {
                if ($genre['zanryid'] != $author['zanryid']) {
                    echo '<option value="' . $genre['zanryid'] . '">' . $genre['nazev'] . '</option>';
                }
            }
            echo '
                </select>
            </div>
            <button type="submit" class="btn btn-lg btn-dark btn-block">
                <i class="fas fa-save"></i>
                Uložit změny
            </button>
        </form>
        ';
    }
    ?>
</section>


<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous">
</script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
</script>
</body>

</html>