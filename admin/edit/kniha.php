<?php

require_once '../../php/Db_connection.php';
require_once '../../php/Session.php';

$session = Session::getInstance();
$redirect = "Location: ../login.php";
$session->adminCheck($redirect);

try {
    $connection = new Db_connection();
} catch (PDOException $e) {
    echo "Connection error: " . $e->getMessage();
}

if (isset($_GET['kniha_id'])) {
    $id = $_GET['kniha_id'];
} else {
    header('Location: ../knihy.php');
}

if (isset($_POST['isbn']) && isset($_POST['nazev']) && isset($_POST['rok']) && isset($_POST['autori']) && isset($_POST['zanry']) && isset($_POST['kategorie']) && isset($_POST['naskladneno'])) {

    // book
    $isbn = $_POST['isbn'];
    $name = $_POST['nazev'];
    $year = $_POST['rok'];
    $categoryid = $_POST['kategorie'];
    $connection->insert("UPDATE knihy SET isbn = '$isbn', nazev = '$name', rokvydani = '$year', kategorieid = '$categoryid' WHERE knihyid = '$id'");

    // book authors
    $selected_authors = array_map('intval', $_POST['autori']);
    $current_authors = $connection->select("SELECT autoriid, knihyid FROM autoriknih WHERE knihyid = '$id'");
    foreach ($selected_authors as $key => $selected_author) {
        foreach ($current_authors as $k => $current_author) {
            if ($current_author['autoriid'] == $selected_author) {
                unset($selected_authors[$key]);
                unset($current_authors[$k]);
            }
        }
    }
    foreach ($current_authors as $current_author) {
        $caid = $current_author['autoriid'];
        $connection->insert("DELETE FROM autoriknih WHERE knihyid = '$id' AND autoriid = '$caid'");
    }
    foreach ($selected_authors as $said) {
        $connection->insert("INSERT INTO autoriknih (autoriid, knihyid) VALUES ('$said', '$id')");
    }

    // book genres
    $selected_genres = array_map('intval', $_POST['zanry']);
    $current_genres = $connection->select("SELECT zanryid, knihyid FROM zanrytitulu WHERE knihyid = '$id'");
    foreach ($selected_genres as $key => $selected_genre) {
        foreach ($current_genres as $k => $current_genre) {
            if ($current_genre['zanryid'] == $selected_genre) {
                unset($selected_genres[$key]);
                unset($current_genres[$k]);
            }
        }
    }
    foreach ($current_genres as $current_genre) {
        $cgid = $current_genre['zanryid'];
        $connection->insert("DELETE FROM zanrytitulu WHERE knihyid = '$id' AND zanryid = '$cgid'");
    }
    foreach ($selected_genres as $sgid) {
        $connection->insert("INSERT INTO zanrytitulu (zanryid, knihyid) VALUES ('$sgid', '$id')");
    }

    // book count
    $count = $_POST['naskladneno'];
    $result = $connection->selectOne("SELECT naskladnenoid FROM knihy WHERE knihyid = '$id'");
    $countid = $result['naskladnenoid'];
    $connection->insert("UPDATE naskladneno SET pocet = '$count' WHERE naskladnenoid = '$countid'");
}

?>

<!doctype html>
<html lang="en" class="h-100">

<head>
    <title>Upravit knihu</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="../../css/all.css">
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/administration.css">
</head>

<body>

<section class="admin-content">
    <?php

    if (isset($id)) {
        $book = $connection->selectOne("SELECT k.knihyid, k.isbn, k.nazev, k.rokvydani, kat.kategorieid, kat.nazev as kategorie, n.pocet FROM knihy k JOIN kategorie kat USING (kategorieid) JOIN naskladneno n USING (naskladnenoid) WHERE k.KnihyID = '$id'");
        $bookid = $book['knihyid'];

        echo '
    <h1 class="mb-3 font-weight-normal text-center">Kniha ' . $book['knihyid'] . '</h1>

    <form action="kniha.php?kniha_id=' . $book['knihyid'] . '" method="post">
        <div class="form-group">
            <label for="knihaIsbn">ISBN</label>
            <input type="text" name="isbn" class="form-control" id="knihaIsbn" value="' . $book['isbn'] . '" required>
        </div>
        <div class="form-group">
            <label for="knihaNazev">Název</label>
            <input type="text" name="nazev" class="form-control" id="knihaNazev" value="' . $book['nazev'] . '" required>
        </div>
        <div class="form-group">
            <label for="knihaNazev">Popis</label>
            <textarea type="text" name="popis" class="form-control" id="knihaPopis" placeholder="Popis" required></textarea>
        </div>
        <div class="form-group">
            <label for="knihaRokVydani">Rok vydání</label>
            <input type="number" name="rok" min="0" max="9999" value="1999" class="form-control" id="' . $book['rokvydani'] . '" required>
        </div>
        <div class="form-group">
            <label for="knihaAutor">Autor</label>
            <select multiple name="autori[]" class="form-control" id="knihaAutor" required>';
        $bookauthors = $connection->select("SELECT autoriid, knihyid FROM autoriknih WHERE knihyid = '$bookid'");
        $authors = $connection->select("SELECT jmeno, prijmeni, autoriid FROM autori ORDER BY prijmeni");
        foreach ($bookauthors as $bookauthor) {
            foreach ($authors as $k => $author) {
                if ($author['autoriid'] == $bookauthor['autoriid']) {
                    echo '<option selected value="' . $author['autoriid'] . '">' . $author['jmeno'] . ' ' . $author['prijmeni'] . '</option>';
                    unset($authors[$k]);
                }
            }
        }
        foreach ($authors as $author) {
            echo '<option value="' . $author['autoriid'] . '">' . $author['jmeno'] . ' ' . $author['prijmeni'] . '</option>';
        }
        echo '
            </select>
        </div>
        <div class="form-group">
            <label for="knihaZanr">Žánr</label>
            <select multiple name="zanry[]" class="form-control" id="knihaZanr" required>';
        $bookgenres = $connection->select("SELECT zanryid, knihyid FROM zanrytitulu WHERE knihyid = '$bookid'");
        $genres = $connection->select("SELECT zanryid, nazev FROM zanry ORDER BY nazev");
        foreach ($bookgenres as $bookgenre) {
            foreach ($genres as $k => $genre) {
                if ($genre['zanryid'] == $bookgenre['zanryid']) {
                    echo '<option selected value="' . $genre['zanryid'] . '">' . $genre['nazev'] . '</option>';
                    unset($genres[$k]);
                }
            }
        }
        foreach ($genres as $genre) {
            echo '<option value="' . $genre['zanryid'] . '">' . $genre['nazev'] . '</option>';
        }
        echo '
            </select>
        </div>
        <div class="form-group">
            <label for="knihaKategorie">Kategorie</label>
            <select name="kategorie" class="form-control" id="knihaKategorie" required>
                <option value="' . $book['kategorieid'] . '">' . $book['kategorie'] . '</option>';
        $categories = $connection->select("SELECT kategorieid, nazev FROM kategorie");
        foreach ($categories as $category) {
            if ($category['kategorieid'] != $book['kategorieid']) {
                echo '<option value="' . $category['kategorieid'] . '">' . $category['nazev'] . '</option>';
            }
        }
        echo '
            </select >
        </div >
        <div class="form-group" >
            <label for="knihaNaskladneno" > Knih na skladě </label >
            <input type = "number" name = "naskladneno" min = "0" max = "9999" value = "' . $book['pocet'] . '" class="form-control" id = "knihaNaskladneno" required >
        </div >
        
        
        <div class="custom-file">
            <input type="file" class="custom-file-input" id="knihaFoto">
            <label class="custom-file-label" for="customFile">Fotografie</label>
        </div>
        
        <button type = "submit" class="btn btn-lg btn-dark btn-block mt-3 mb-3" >
            <i class="fas fa-save" ></i >
        Uložit změny
        </button >
        </form >
    ';
    }
    ?>

</section>


<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous">
</script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
</script>
</body>

</html>