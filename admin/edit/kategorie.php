<?php

require_once '../../php/Db_connection.php';
require_once '../../php/Session.php';

$session = Session::getInstance();
$redirect = "Location: ../login.php";
$session->adminCheck($redirect);

try {
    $connection = new Db_connection();
} catch (PDOException $e) {
    echo "Connection error: " . $e->getMessage();
}

if (isset($_GET['kategorie_id'])) {
    $id = $_GET['kategorie_id'];
} else {
    header('Location: ../kategorie.php');
}

if (isset($_POST['nazev'])) {
    $name = $_POST['nazev'];
    $connection->insert("UPDATE kategorie SET nazev='$name' WHERE kategorieid = '$id'");
}

?>

<!doctype html>
<html lang="en" class="h-100">

<head>
    <title>Upravit kategorii</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="../../css/all.css">
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/administration.css">
</head>

<body>

<section class="admin-content">
    <?php

    if (isset($id)) {
    $category = $connection->selectOne("SELECT kategorieid, nazev FROM kategorie WHERE kategorieid = '$id'");
    echo '
    <h1 class="mb-3 font-weight-normal text-center">Kategorie ' . $category['kategorieid'] . '</h1>

    <form action="kategorie.php?kategorie_id=' . $category['kategorieid'] . '" method="post">
        <div class="form-group mb-3">
            <label for="autorJmeno">Jméno kategorie</label>
            <input type="text" name="nazev" class="form-control" id="kategorieJmeno" value="' . $category['nazev'] . '">
        </div>
        <button type="submit" class="btn btn-lg btn-dark btn-block">
            <i class="fas fa-save"></i>
            Uložit změny
        </button>

    </form>
    ';
    }
    ?>
</section>


<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous">
</script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
</script>
</body>

</html>