<?php

require_once '../php/Db_connection.php';
require_once '../php/Session.php';

$session = Session::getInstance();
try {
    $connection = new Db_connection();
} catch (PDOException $e) {
    echo "Connection error: " . $e->getMessage();
}

if (isset($_POST['inputPassword'])) {
    $code = $_POST['inputPassword'];
    $result = $connection->selectOne("SELECT zamestnanecid FROM zamestnanec WHERE kod ='$code'");
    if (count($result) > 0) {
        $_SESSION['isAdmin'] = 1;
    }
}

$redirect = "Location: login.php";
$session->adminCheck($redirect);

?>

<!doctype html>
<html lang="en" class="h-100">

<head>
    <title>Administration login</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="../css/all.css">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/administration.css">
</head>

<body class="text-center">

<section class="admin-content">
    <h1 class="mb-3 font-weight-normal">Vítej <br>Admine Adminoviči</h1>
    <nav>
        <ul class="list-group">
            <li class="list-group-item"><a href="knihy.php">Knihy</a></li>
            <li class="list-group-item"><a href="autori.php">Autoři</a></li>
            <li class="list-group-item"><a href="kategorie.php">Kategorie</a></li>
            <li class="list-group-item"><a href="zakaznici.php">Zákazníci</a></li>
            <li class="list-group-item"><a href="objednavky.php">Objednávky</a></li>
            <li class="list-group-item list-group-item-danger"><a class="text-dark" href="../index.php">Zpět na web</a>
            </li>
        </ul>
    </nav>
</section>


<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous">
</script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
</script>
</body>

</html>