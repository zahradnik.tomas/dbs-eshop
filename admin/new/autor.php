<?php

require_once '../../php/Db_connection.php';
require_once '../../php/Session.php';

$session = Session::getInstance();
$redirect = "Location: ../login.php";
$session->adminCheck($redirect);

try {
    $connection = new Db_connection();
} catch (PDOException $e) {
    echo "Connection error: " . $e->getMessage();
}

if (isset($_POST['jmeno'])) {
    $name = $_POST['jmeno'];
    $surname = $_POST['prijmeni'];
    $birth = $_POST['rok'];
    $genre = $_POST['zanr'];

    $connection->insert("INSERT INTO autori (jmeno, prijmeni, roknarozeni, zanryid) VALUES ('$name', '$surname', '$birth', '$genre')");
}

?>

<!doctype html>
<html lang="en" class="h-100">

<head>
    <title>Přidat autora</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="../../css/all.css">
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/administration.css">
</head>

<body>

<section class="admin-content">
    <h1 class="mb-3 font-weight-normal text-center">Přidat autora</h1>

    <form action="autor.php" method="post">
        <div class="form-group">
            <label for="autorJmeno">Jméno</label>
            <input type="text" name="jmeno" class="form-control" id="autorJmeno" placeholder="Jméno" required>
        </div>
        <div class="form-group">
            <label for="autorPrijmeni">Prijmení</label>
            <input type="text" name="prijmeni" class="form-control" id="autorPrijmeni" placeholder="Přijmení" required>
        </div>
        <div class="form-group">
            <label for="autorRokNarozeni">Rok narození</label>
            <input type="number" name="rok" class="form-control" id="autorRokNarozeni" required>
        </div>
        <div class="form-group">
            <label for="autorZanr">Žánr</label>
            <select name="zanr" class="form-control" id="autorZanr" required>
                <?php
                $genres = $connection->select("SELECT zanryid, nazev FROM zanry");
                foreach ($genres as $genre) {
                    echo '<option value="' . $genre['zanryid'] . '">' . $genre['nazev'] . '</option>';
                }
                ?>
            </select>
        </div>
        <button type="submit" class="btn btn-lg btn-dark btn-block">
            <i class="fas fa-plus-circle"></i>
            Přidat
        </button>

    </form>
</section>


<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous">
</script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
</script>
</body>

</html>