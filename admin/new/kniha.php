<?php

require_once '../../php/Db_connection.php';
require_once '../../php/Session.php';

$session = Session::getInstance();
$redirect = "Location: ../login.php";
$session->adminCheck($redirect);

try {
    $connection = new Db_connection();
} catch (PDOException $e) {
    echo "Connection error: " . $e->getMessage();
}

//if (isset($_POST['isbn'])) {
//    $price = ;
//    $picture = ;
//    $isbn = $_POST['isbn'];
//    $name = $_POST['nazev'];
//    $description = ;
//    $year = $_POST['rok'];
//    $category = $_POST['kategorie'];
//    $count = $_POST['naskladneno'];
//
//    $sql = "CALL add_kniha('$price', '$picture','$isbn', '$name', '$description', '$year', '$category', '$count')";
//    $connection->insert("$sql");
//    $id = $connection->selectOne("SELECT knihyid, isbn FROM knihy WHERE isbn = '$isbn'");
//
//    $authors = array_map('intval', $_POST['autori']);
//    foreach ($authors as $aid) {
//        $connection->insert("INSERT INTO autoriknih (autoriid, knihyid) VALUES ('$aid', '$id')");
//    }
//
//    $genres = array_map('intval', $_POST['zanry']);
//    foreach ($genres as $gid) {
//        $connection->insert("INSERT INTO zanrytitulu (zanryid, knihyid) VALUES ('$gid', '$id')");
//    }
//}

?>

<!doctype html>
<html lang="en" class="h-100">

<head>
    <title>Přidat knihu</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="../../css/all.css">
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/administration.css">
</head>

<body>

<section class="admin-content">
    <h1 class="mb-3 font-weight-normal text-center">Přidat knihu</h1>
    <form action="kniha.php" method="post">
        <div class="form-group">
            <label for="knihaIsbn">ISBN</label>
            <input type="text" name="isbn" class="form-control" id="knihaIsbn" placeholder="ISBN" required>
        </div>
        <div class="form-group">
            <label for="knihaNazev">Název</label>
            <input type="text" name="nazev" class="form-control" id="knihaNazev" placeholder="Název" required>
        </div>
        <div class="form-group">
            <label for="knihaNazev">Popis</label>
            <textarea type="text" name="popis" class="form-control" id="knihaPopis" placeholder="Popis" required></textarea>
        </div>
        <div class="form-group">
            <label for="knihaRokVydani">Rok vydání</label>
            <input type="number" name="rok" min="0" max="9999" placeholder="Rok vydání" class="form-control"
                   id="knihaRokVydani" required>
        </div>
        <div class="form-group">
            <label for="knihaAutor">Autor</label>
            <select multiple name="autori[]" class="form-control" id="knihaAutor" required>
                <?php
                $authors = $connection->select("SELECT jmeno, prijmeni, autoriid FROM autori");
                foreach ($authors as $author) {
                    echo '<option value="' . $author['autoriid'] . '">' . $author['jmeno'] . ' ' . $author['prijmeni'] . '</option>';
                }
                ?>
            </select>
        </div>
        <div class="form-group">
            <label for="knihaZanr">Žánr</label>
            <select multiple name="zanry[]" class="form-control" id="knihaZanr" required>
                <?php
                $genres = $connection->select("SELECT zanryid, nazev FROM zanry");
                foreach ($genres as $genre) {
                    echo '<option value="' . $genre['zanryid'] . '">' . $genre['nazev'] . '</option>';
                }
                ?>
            </select>
        </div>
        <div class="form-group">
            <label for="knihaKategorie">Kategorie</label>
            <select name="kategorie" class="form-control" id="knihaKategorie" required>
                <?php
                $categories = $connection->select("SELECT kategorieid, nazev FROM kategorie");
                foreach ($categories as $category) {
                    echo '<option value="' . $category['kategorieid'] . '">' . $category['nazev'] . '</option>';
                }
                ?>
            </select>
        </div>
        <div class="form-group">
            <label for="knihaNaskladneno">Knih na skladě</label>
            <input type="number" name="naskladneno" min="0" max="9999" placeholder="Knih na skladě" class="form-control"
                   id="knihaNaskladneno" required>
        </div>

        <div class="custom-file">
            <input type="file" class="custom-file-input" id="knihaFoto">
            <label class="custom-file-label" for="customFile">Fotografie</label>
        </div>

        <button type="submit" class="btn btn-lg btn-dark btn-block mt-3 mb-3">
            <i class="fas fa-plus-circle"></i>
            Přidat
        </button>
    </form>
</section>

<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous">
</script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
</script>
</body>

</html>