<?php

require_once '../php/Db_connection.php';
require_once '../php/Session.php';

$session = Session::getInstance();
$redirect = "Location: login.php";
$session->adminCheck($redirect);

try {
    $connection = new Db_connection();
} catch (PDOException $e) {
    echo "Connection error: " . $e->getMessage();
}

if (isset($_GET['smazat_zakaznika'])) {
    $id = $_GET['smazat_zakaznika'];
    $connection->insert("DELETE FROM zakaznici WHERE zakazniciid = '$id'");
}

?>

<!doctype html>
<html lang="en" class="h-100">

<head>
    <title>Zákazníci</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="../css/all.css">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/administration.css">
</head>

<body class="text-center">

<section class="admin-content admin-content-table">
    <h1 class="mb-3 font-weight-normal">Zákazníci</h1>

    <!-- SEARCH BAR -->
    <?php
    include '../php/components.php';
    echo getSearchBar("mb-3", "Zákazníci");
    ?>

    <!-- TABLE -->
    <table class="table table-sm table-striped table-responsive-sm">
        <thead>
        <tr>
            <th>ID</th>
            <th>Jmeno</th>
            <th>Prijmeni</th>
            <th>Email</th>
            <th>Telefon</th>
            <th>Datum narozeni</th>
            <th class="col-edit">Detail</th>
            <th class="col-delete">Odstranit</th>
        </tr>
        </thead>
        <tbody>
        <?php

        $result = $connection->select("SELECT zakazniciid, jmeno, prijmeni, email, telefon, datumnarozeni FROM zakaznici ORDER BY prijmeni");

        if (count($result) > 0) {
            foreach ($result as $row) {
                echo '
                    <tr>
                        <td>' . $row['zakazniciid'] . '</td>
                        <td>' . $row['jmeno'] . '</td>
                        <td>' . $row['prijmeni'] . '</td>
                        <td>' . $row['email'] . '</td>
                        <td>' . $row['telefon'] . '</td>
                        <td>' . date("Y-m-d", strtotime($row['datumnarozeni'])) . '</td>
                        <td><a href="show/zakaznik.php?zakaznik_id=' . $row['zakazniciid'] . '"><i class="fas fa-info-circle fa-lg text-dark"></i></a></td>
                        <td><a href="zakaznici.php?smazat_zakaznika=' . $row['zakazniciid'] . '"><i class="fas fa-trash"></i></a></td>
                    </tr>
                        ';
            }
        }

        ?>
        </tbody>
    </table>

    <!-- PAGINATION -->
    <nav aria-label="Page navigation example">
        <ul class="pagination justify-content-center">
            <li class="page-item disabled">
                <a class="page-link" href="#" tabindex="-1">Previous</a>
            </li>
            <li class="page-item active">
                <a class="page-link" href="#">1</a>
            </li>
            <li class="page-item">
                <a class="page-link" href="#">2</a>
            </li>
            <li class="page-item">
                <a class="page-link" href="#">3</a>
            </li>
            <li class="page-item">
                <a class="page-link" href="#">Next</a>
            </li>
        </ul>
    </nav>
    <a href="admin-menu.php"><i class="fas fa-arrow-alt-circle-left"></i> Zpět</a>
</section>


<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous">
</script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
</script>
</body>

</html>