<?php

require_once '../php/Db_connection.php';
require_once '../php/Session.php';

$session = Session::getInstance();

?>

<!doctype html>
<html lang="en" class="h-100">

<head>
    <title>Administration login</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="../css/all.css">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/administration.css">
</head>

<body class="text-center">
    <form class="form-signin" action="admin-menu.php" method="post">
        <h1 class="mb-3 font-weight-normal">Administrace</h1>
        <label for="inputPassword" class="sr-only">Password</label>
        <input id="inputPassword" type="password" name="inputPassword" class="form-control mb-2" placeholder="Heslo"
            required>

        <button type="submit" class="btn btn-lg btn-dark btn-block">Přihlásit se</button>
        <a class="btn btn-block list-group-item-danger text-dark" href="../index.php">Zpět na web</a>
    </form>

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
    </script>
</body>

</html>