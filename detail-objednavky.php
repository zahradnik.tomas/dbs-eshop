<?php

require_once 'php/Db_connection.php';
require_once 'php/Session.php';
require_once 'php/components.php';

try {
    $connection = new Db_connection();
} catch (PDOException $e) {
    echo "Connection error: " . $e->getMessage();
}

?>

<!doctype html>
<html lang="en">

<head>
    <title>Detail objenávky</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="css/all.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/custom.css">
</head>

<body>

<!-- HEADER -->
<header>
    <nav class="navbar navbar-expand-md navbar-dark bg-dark">
        <!-- USER  -->
        <div class="pos-f-t container d-md-none">
            <div class="collapse" id="navbarToggleUserMenu">
                <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                    <li class="nav-item active">
                        <a class="nav-link" href="udaje.php">Moje údaje</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="objednavky.php.php">Objednávky</a>
                    </li>

                    <li class="nav-item">
                        <a href="kosik.php" class="nav-link active">
                            <i class="fa-shopping-basket fas fa-lg"></i>
                            Košík
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="index.php">
                            <i class="fas fa-sign-out-alt"></i>
                            Odhlásit se
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="container">
            <!-- Logo -->
            <a class="navbar-brand" href="index.php">Knihomol</a>
            <!-- Nav controll icons -->
            <div class="btn-group">
                <button class="navbar-toggler navbar-user-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarToggleUserMenu" aria-controls="navbarToggleUserMenu" aria-expanded="false"
                        aria-label="Toggle navigation">
                    <i class="fas fa-user"></i>
                </button>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavId"
                        aria-controls="collapsibleNavId" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </div>
            <!-- MAIN NAV COLLAPSE -->
            <div class="collapse navbar-collapse" id="collapsibleNavId">
                <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                    <li class="nav-item">
                        <a class="nav-link" href="index.php">Domů <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="dropdownId" data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false">Kategorie</a>
                        <div class="dropdown-menu" aria-labelledby="dropdownId">
                            <a class="dropdown-item" href="#">Román</a>
                            <a class="dropdown-item" href="#">Krimi</a>
                            <a class="dropdown-item" href="#">Sci-fi</a>
                            <a class="dropdown-item" href="#">Dokumentární</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">Všechny Kategorie</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Kontakt</a>
                    </li>
                </ul>

                <ul class="navbar-nav ml-auto d-md-flex d-sm-none">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-user"></i>
                            Jmeno Zakaznika
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="udaje.php">Moje údaje</a>
                            <a class="dropdown-item" href="objednavky.php">Objednávky</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="index.php">
                                <i class="fas fa-sign-out-alt"></i>
                                Odhlásit se
                            </a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a href="kosik.php" class="nav-link active">
                            <i class="fa-shopping-basket fas fa-lg"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>


<section id="heading-section" class="bg-red text-white">

    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-6">
                <h2>Detail objednávky</h2>
            </div>
        </div>
    </div>

</section>

<section class="admin-content admin-content-table pt-4">
    <div class="container" >
        <dl class="row text-left">
            <dt class="col-sm-4">ID:</dt>
            <dd class="col-sm-8">' . $order['objednavkaid'] . '</dd>

            <dt class="col-sm-4">Objednáno:</dt>
            <dd class="col-sm-8">' . date("Y-m-d H:i:s", strtotime($order['datum'])) . '</dd>

            <dt class="col-sm-4">Cena:</dt>
            <dd class="col-sm-8">' . $order['zpusob'] . '</dd>

            <dt class="col-sm-4">Typ platby:</dt>
            <dd class="col-sm-8">' . $order['zpusob'] . '</dd>

            <dt class="col-sm-4">Vyrizeno:</dt>
            <dd class="col-sm-8">' . $order['jmeno'] . ' ' . $order['prijmeni'] . '</dd>

            <dt class="col-sm-4">Ulice, č.p.:</dt>
            <dd class="col-sm-8">' . $order['jmeno'] . ' ' . $order['prijmeni'] . '</dd>

            <dt class="col-sm-4">Město:</dt>
            <dd class="col-sm-8">' . $order['jmeno'] . ' ' . $order['prijmeni'] . '</dd>

            <dt class="col-sm-4">Telefon:</dt>
            <dd class="col-sm-8">' . $order['jmeno'] . ' ' . $order['prijmeni'] . '</dd>

        </dl>

        <h2>Zakoupené položky</h2>
        <?php
        if (isset($id)) {
        $order = $connection->selectOne("SELECT o.objednavkaid, o.datum, o.zakazniciid, p.zpusob, z.jmeno, z.prijmeni FROM objednavka o INNER JOIN platba p USING (platbaid) INNER JOIN zamestnanec z USING (zamestnanecid) WHERE o.objednavkaid = '$id'");
        echo '
        <table class="table table-sm table-striped table-responsive-sm">
        <thead>
        <tr>
            <th>ID</th>
            <th>Název</th>
            <th class="col-edit">Detail</th>
            <th>Cena</th>
            <th>Počet kusů</th>
        </tr>
        </thead>
        <tbody>';

        $oid = $order['objednavkaid'];
        $books = $connection->select("SELECT p.pocet, p.objednavkaid, k.knihyid, k.nazev, k.cena FROM polozky p INNER JOIN knihy k USING (knihyid) WHERE p.objednavkaid = '$oid'");

        if (count($books) > 0) {
            foreach ($books as $book) {
                echo '
        <tr>
            <th>' . $book['knihyid'] . '</th>
            <td>' . $book['nazev'] . '</td>
            <td><a href="../edit/kniha.php?kniha_id=' . $book['knihyid'] . '"><i class="fas fa-pencil-alt"></i></a></td>
            <td>' . $book['cena'] . '</td>
            <td>' . $book['pocet'] . '</td>
        </tr>
                ';
            }
        }
        echo '
        </tbody>
    </table>
        ';
    }
    ?>
        ?>
    </div>
</section>


<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous">
</script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
</script>
</body>

</html>