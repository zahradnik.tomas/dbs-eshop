<?php

require_once 'php/Db_connection.php';
require_once 'php/Session.php';
require_once 'php/components.php';

try {
    $connection = new Db_connection();
} catch (PDOException $e) {
    echo "Connection error: " . $e->getMessage();
}

?>

<!doctype html>
<html lang="en">

<head>
    <title>Eshop</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="css/all.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/custom.css">
</head>

<body>

    <!-- HEADER -->
    <header>
        <nav class="navbar navbar-expand-md navbar-dark bg-dark">
            <!-- USER  -->
            <div class="pos-f-t container d-md-none">
                <div class="collapse" id="navbarToggleUserMenu">
                    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                        <li class="nav-item">
                            <a class="nav-link" href="login.php">Přihlásit se</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="register.php">Registrovat</a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="container">
                <!-- Logo -->
                <a class="navbar-brand" href="index.php">Knihomol</a>
                <!-- Nav controll icons -->
                <div class="btn-group">
                    <button class="navbar-toggler navbar-user-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarToggleUserMenu" aria-controls="navbarToggleUserMenu" aria-expanded="false"
                        aria-label="Toggle navigation">
                        <i class="fas fa-user"></i>
                    </button>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavId"
                        aria-controls="collapsibleNavId" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                </div>
                <!-- MAIN NAV COLLAPSE -->
                <div class="collapse navbar-collapse" id="collapsibleNavId">
                    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                        <li class="nav-item active">
                            <a class="nav-link" href="index.php">Domů <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="dropdownId" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">Kategorie</a>
                            <div class="dropdown-menu" aria-labelledby="dropdownId">
                                <a class="dropdown-item" href="#">Román</a>
                                <a class="dropdown-item" href="#">Krimi</a>
                                <a class="dropdown-item" href="#">Sci-fi</a>
                                <a class="dropdown-item" href="#">Dokumentární</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#">Všechny Kategorie</a>
                            </div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Kontakt</a>
                        </li>
                    </ul>

                    <ul class="navbar-nav ml-auto d-md-flex d-sm-none">
                        <li class="nav-item">
                            <a class="nav-link" href="login.php">Přihlásit se</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="register.php">Registrovat</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>


    <section id="showcase" class="bg-red">
        <div class="container">
            <div class="form-group">
                <label class="h2 text-white" for="book-search">Prohledejte naši knihovnu</label>
                <!-- SEARCH BAR -->
                <?php
                    echo getSearchBar("", "Knihy", "Autoři");
                ?>
            </div>
        </div>
    </section>

<section id="latest-releases">
    <div class="container pt-3 mb-4">
        <h2>Mohlo by se Vám líbit</h2>
        <div class="row text-center">
            <?php

            $books = $connection->select("SELECT nazev, popis FROM knihy");

            if (count($books) > 0) {
                foreach ($books as $book) {
                    echo '
                            <div class="col-sm-12 col-md-6 col-lg-3 mb-2">
                                <div class="card">
                                    <img class="card-img-top" src="https://dummyimage.com/250x180/000/fff" alt="">
                                    <div class="card-body">
                                        <h6 class="card-title">' . $book['nazev'] . '</h6>
                                    </div>
                                </div>
                            </div>
                            ';
                }
            }

            ?>
        </div>
    </div>
</section>

<section id="browse">
    <div class="container pt-4 text-white">
        <div class="row">
            <div class="col-sm-12 col-md-6">
                <span class="h5">Kategorie</span>
                <ul>
                    <?php

                    $categories = $connection->select("SELECT nazev FROM kategorie");

                    if (count($categories) > 0) {
                        foreach ($categories as $category) {
                            echo '
                            <li>' . $category['nazev'] . '</li>
                            ';
                        }
                    }

                    ?>
                </ul>
            </div>
            <div class="col-sm-12 col-md-6">
                <span class="h5 text-white">Autoři</span>
                <ul>
                    <?php

                    $authors = $connection->select("SELECT jmeno, prijmeni FROM autori");

                    if (count($authors) > 0) {
                        foreach ($authors as $author) {
                            echo '<li>' . $author['jmeno'] . ' ' . $author['prijmeni'] . '</li>';
                        }
                    }

                    ?>
                </ul>
            </div>
        </div>
    </div>
</section>

<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous">
</script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
</script>
</body>

</html>