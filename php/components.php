<?php
//function getHtmlHead($secondaryHeading = "", $heading = "Knihomol", $){
//    $head =
//        '<head>
//        <title>' . $heading . ' | ' . $secondaryHeading . '</title>
//        <meta charset="utf-8">
//        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
//        ' . getCssLinks($heading) . '
//        ';
//
//    return $head;
//}
//
//function getCssLinks($heading){
//    if ($heading == "Knihomol"){
//        $css =
//            '<link rel="stylesheet" href="css/all.css">
//            <link rel="stylesheet" href="css/bootstrap.min.css">
//            <link rel="stylesheet" href="css/custom.css">
//            ';
//    } else {
//        $css =
//            '<link rel="stylesheet" href="../../css/all.css">
//            <link rel="stylesheet" href="../../css/bootstrap.min.css">
//            <link rel="stylesheet" href="../../css/administration.css">
//            ';
//    }
//    return $css;
//}
//
//function getBootstrapScripts(){
//    $scripts = '
//        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
//            integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous">
//        </script>
//        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
//            integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
//        </script>
//        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
//            integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
//        </script>
//    ';
//
//    return $scripts;
//}


// SEARCH COMPONENT

function getSearchBar($classes = "", $primarySearch = "", $secondarySearch = "")
{
    return $searchBar =
        '<div class="input-group ' . $classes . '">
                <input type="text" class="form-control" name="book-search" id="book-search" aria-describedby="helpId"
                       placeholder="Vyhledávání..">

                ' . ($secondarySearch == "" ? '' : '
                    <div class="input-group-append">
                        <button class="btn btn-success" type="submit"><i class="fas fa-search"></i> ' . $secondarySearch . '</button>
                    </div>
                ') . '
               <div class="input-group-append">
                    <button class="btn btn-dark" type="submit"><i class="fas fa-search"></i> ' . $primarySearch . '</button>
                </div>
            </div>
        ';
}

function getAddButton($title = "", $href = "", $clases = "")
{
    return '<a href="' . $href . '" class="btn btn-outline-primary float-right' . $clases . '">
    <i class="fas fa-plus-circle"></i>
    Přidat ' . $title . '</a>';
}

