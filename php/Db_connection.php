<?php

class Db_connection
{
    public PDO $pdo;

    function __construct()
    {
        try {
            $servername = "localhost";
            $dbname = "retardi";
            $db_username = "root";
            $db_password = "password";
            $dsn = "mysql:host=$servername;dbname=$dbname";

            $this->pdo = new PDO($dsn, $db_username, $db_password, array(
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_EMULATE_PREPARES => false,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC));

        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function select($sql)
    {
        $sql_stmt = $this->pdo->prepare($sql);
        $sql_stmt->execute();
        return $sql_stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function selectOne($sql)
    {
        $sql_stmt = $this->pdo->prepare($sql);
        $sql_stmt->execute();
        return $sql_stmt->fetch(PDO::FETCH_ASSOC);
    }

    public function insert($sql)
    {
        $sql_stmt = $this->pdo->prepare($sql);
        try {
            $result = $sql_stmt->execute();
        } catch (PDOException $e) {
            trigger_error('Error occurred while trying to insert into the DB:' . $e->getMessage(), E_USER_ERROR);
        }
        if ($result) {
            return $sql_stmt->rowCount();
        }
    }
}