<?php

class Session
{
    const SESSION_STARTED = TRUE;
    const SESSION_NOT_STARTED = FALSE;
    private $sessionState = self::SESSION_NOT_STARTED;
    private static $instance;

    private function __construct()
    {
    }

    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new self;
        }

        self::$instance->startSession();

        return self::$instance;
    }

    public function startSession()
    {
        if ($this->sessionState == self::SESSION_NOT_STARTED) {
            $this->sessionState = session_start();
        }

        return $this->sessionState;
    }

    public function set($key, $value)
    {
        $_SESSION[$key] = $value;
    }

    public function get($key)
    {
        if (isset($_SESSION[$key])) {
            return $_SESSION[$key];
        }
    }


    public function isset($key)
    {
        return isset($_SESSION[$key]);
    }


    public function unset($key)
    {
        unset($_SESSION[$key]);
    }

    public function adminCheck($redirect) {
        if (isset($_SESSION['isAdmin'])) {
            if ($this->get($_SESSION['isAdmin'] != 1)) {
                header($redirect);
            }
        } else {
            header($redirect);
        }
    }

    public function destroy()
    {
        if ($this->sessionState == self::SESSION_STARTED) {
            $this->sessionState = !session_destroy();
            unset($_SESSION);

            return !$this->sessionState;
        }

        return FALSE;
    }
}