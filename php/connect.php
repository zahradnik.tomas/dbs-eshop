<?php

class db_connection {
    public PDO $pdo;

    function __construct() {
        try {
            $server = "ora1.uhk.cz";
            $db_username = "retardi";
            $db_password = "retardi";
            $service_name = "ORCL";
            $sid = "ORCL";
            $port = 1521;
            $dsn = "oci:dbname=(DESCRIPTION = (ADDRESS = (PROTOCOL = TCP)(HOST = $server)(PORT = $port)) (CONNECT_DATA = (SERVICE_NAME = $service_name) (SID = $sid)));charset=utf8";

            $this->pdo = new PDO($dsn, $db_username, $db_password, array(
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_EMULATE_PREPARES => false,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC));

        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function select($sql) {
        $sql_stmt = $this->pdo->prepare($sql);
        $sql_stmt->execute();
        return $sql_stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function insert($sql) {
        $sql_stmt = $this->pdo->prepare($sql);
        try {
            $result = $sql_stmt->execute();
        } catch (PDOException $e) {
            trigger_error('Error occurred while trying to insert into the DB:' . $e->getMessage(), E_USER_ERROR);
        }
        if ($result) {
            return $sql_stmt->rowCount();
        }
    }
}